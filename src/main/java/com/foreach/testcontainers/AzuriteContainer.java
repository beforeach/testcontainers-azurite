package com.foreach.testcontainers;

import com.microsoft.azure.storage.CloudStorageAccount;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.experimental.FieldDefaults;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.utility.TestcontainersConfiguration;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.function.Supplier;

public class AzuriteContainer extends GenericContainer<AzuriteContainer>
{
    public static final String VERSION = "3.7.0";
    private static final CloudStorageAccount developmentStorageAccount = CloudStorageAccount.getDevelopmentStorageAccount();

    public AzuriteContainer() {
        this(VERSION);
    }

    public AzuriteContainer(String version) {
        super( TestcontainersConfiguration.getInstance().getProperties().getOrDefault( "azurite.container.image",
                "mcr.microsoft.com/azure-storage/azurite" ) + ":" + version );
    }

    public CloudStorageAccount storageAccount() {
        URI blob = fromUri( developmentStorageAccount::getBlobEndpoint, () -> Service.BLOB ); //10000
        URI queue = fromUri( developmentStorageAccount::getQueueEndpoint, () -> Service.QUEUE ); //10001
        return new CloudStorageAccount( developmentStorageAccount.getCredentials(), blob, queue, developmentStorageAccount.getTableEndpoint() );
    }

    @SneakyThrows
    private URI fromUri( Supplier<URI> endpoint, Supplier<Service> service ) {
        URI endpointURI = endpoint.get();
        return new URI(endpointURI.getScheme(), endpointURI.getUserInfo(), getContainerIpAddress(), getMappedPort( service.get().port ), endpointURI.getPath(), endpointURI.getQuery(), endpointURI.getFragment());
    }

    @RequiredArgsConstructor
    @Getter
    @FieldDefaults(makeFinal = true)
    public enum Service
    {
        BLOB( 10000 ),
        QUEUE( 10001 );

        int port;
    }
}
